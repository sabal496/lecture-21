package com.example.lectruce21.Adapters

import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lectruce21.Interfaces.callbackid
import com.example.lectruce21.Models.MyModel
import com.example.lectruce21.R
import kotlinx.android.synthetic.main.list_recycle.view.*

class Adapter(val mylist:MutableList<MyModel>, val callsback: callbackid): RecyclerView.Adapter<Adapter.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        return holder(LayoutInflater.from(parent.context).inflate(R.layout.list_recycle,parent,false))
    }

    override fun getItemCount(): Int {
        return mylist.size
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.onbind()
    }

    inner class holder(itemView: View): RecyclerView.ViewHolder(itemView){
        private  lateinit var model: MyModel

        fun onbind(){
            model=mylist[adapterPosition]

            itemView.usersname.text="Name : "+model.name
            itemView.job.text="job : "+model.job
            itemView.idd.text="id : "+model.id
            itemView.date.text="created date : "+model.createdAt

            d("namelog",model.name)
            itemView.editbtn.setOnClickListener(){
                callsback.getuser(adapterPosition)
            }
        }




    }
}