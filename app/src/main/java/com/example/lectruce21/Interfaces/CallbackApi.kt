package com.example.lectruce21.Interfaces

interface CallbackApi {
    fun onResponse(value:String?)
    fun onFailure(value:String?)
}