package com.example.lectruce21.Activities

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.annotation.RequiresApi
import com.example.lectruce21.Api.ApiRequest
import com.example.lectruce21.Interfaces.CallbackApi
import com.example.lectruce21.R
import kotlinx.android.synthetic.main.activity_edituser.*
import kotlinx.android.synthetic.main.list_recycle.editbtn

class edituser : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edituser)
        init()
    }
    private fun init(){
        var intentt=intent
        var intent=intent.extras
        var namee=intent?.getString("name")
        var jobb=intent?.getString("job")
        var id=intent?.getInt("id")
        name.setText(namee)
        jobe.setText(jobb)
        editbtn.setOnClickListener(){



            val map= mutableMapOf<String,String>()
            map["name"]=name.text.toString()
            map["job"]=jobe.text.toString()

            ApiRequest.pusRequest("users",id,object :CallbackApi{
                override fun onResponse(value: String?) {
                    intentt.putExtra("newname",name.text.toString())
                    intentt.putExtra("newjob",jobe.text.toString())
                    setResult(Activity.RESULT_OK,intentt)
                    finish()
                }

                override fun onFailure(value: String?) {

                 }
            },map,this)




        }


    }
}
