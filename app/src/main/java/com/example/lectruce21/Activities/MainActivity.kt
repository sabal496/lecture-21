package com.example.lectruce21.Activities

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.lectruce21.*
import com.example.lectruce21.Api.ApiRequest
import com.example.lectruce21.Interfaces.CallbackApi
import com.example.lectruce21.Models.MyModel
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject


val userslist= mutableListOf<MyModel>()
val userslist2= mutableListOf<MyModel>()
//lateinit var list:ArrayList<MyModel>
val list = arrayListOf<MyModel>()

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        creatbtn.setOnClickListener(){
            createUser()
        }
        lisusers.setOnClickListener(){
            (0 until userslist.size).forEach{
                it->
                if(userslist[it].id!="" && userslist[it].createdAt!=""){
                   list.add(userslist[it])
                     d("ragaca", list[it].id)
                }
            }
            var intent=Intent(this, recycleview::class.java)
            var bundle: Bundle= Bundle()
            bundle.putParcelableArrayList("model",
                list
            )
            intent.putExtra("mymodel", bundle)
            startActivity(intent)
        }
    }

    private  fun createUser(){
        creatbtn.isClickable=false
            val namee=name.text.toString()
            val jobe=job.text.toString()
            val params= mutableMapOf<String,String>()
            params["name"]=namee
            params["job"]=jobe
            userslist.add(
                MyModel(
                    namee,
                    jobe,
                    "",
                    ""
                )
            )
        ApiRequest.posRequest(
            "users",
            object : CallbackApi {
                override fun onResponse(value: String?) {
                    creatbtn.isClickable = true
                    val jsonobj = JSONObject(value)
                    lateinit var id: String
                    lateinit var cratedat: String
                    if (jsonobj.has("id"))
                        id = jsonobj.getString("id")
                    if (jsonobj.has("createdAt"))
                        cratedat = jsonobj.getString("createdAt")
                    (0 until userslist.size).forEach() { it ->
                        if (userslist[it].id == "" || userslist[it].createdAt == "") {
                            userslist[it].id =
                                id
                            userslist[it].createdAt =
                                cratedat
                        }


                    }

                    Toast.makeText(this@MainActivity, "user was created", Toast.LENGTH_SHORT).show()
                    name.text.clear()
                    job.text.clear()


                }

                override fun onFailure(value: String?) {
                }
            },
            params,
            this
        )
    }
}
