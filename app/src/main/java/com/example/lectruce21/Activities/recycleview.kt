package com.example.lectruce21.Activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lectruce21.Adapters.Adapter
import com.example.lectruce21.Models.MyModel
import com.example.lectruce21.R
import com.example.lectruce21.Interfaces.callbackid
import kotlinx.android.synthetic.main.activity_recycleview.*

class recycleview : AppCompatActivity() {

    lateinit var adapter: Adapter
    var REQUEST_CODE=1
    var posit=0
    var listdata :ArrayList<MyModel>?= arrayListOf<MyModel>()
    var ContentList = mutableListOf<MyModel>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycleview)
        init()
    }

    private fun init(){

       var intent=intent.extras
       var bundle= intent?.getBundle("mymodel")
        listdata=bundle?.getParcelableArrayList<MyModel>("model")
        (0 until list.size).forEach(){
            it->
            ContentList.add(list[it])
        }
        adapter= Adapter(ContentList,
            object : callbackid {
                override fun getuser(position: Int) {
                    var intentt = Intent(
                        this@recycleview,
                        edituser::class.java
                    )

                    intentt.putExtra("name", ContentList?.get(position)?.name)
                    intentt.putExtra("job", ContentList?.get(position)?.job)
                    intentt.putExtra("id", ContentList?.get(position)?.id)

                    startActivityForResult(intentt, REQUEST_CODE)
                    posit = position
                }
            })
        recycle.layoutManager=LinearLayoutManager(this)
        recycle.adapter=adapter

         }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(requestCode==REQUEST_CODE && resultCode== Activity.RESULT_OK){

            var inte=data!!.extras
            var model=ContentList?.get(posit)
            model?.name =data?.extras?.getString("newname")
            model?.job =data?.extras?.getString("newjob")

            adapter.notifyDataSetChanged()


            d("change", listdata?.get(posit)?.name)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

}
